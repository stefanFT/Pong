
// (posX, posY) is the upper left corner of the rectangle.
function createRectangle(width, height, posX = 0, posY = 0) {
  var _rectangle = {};

  _rectangle.width = width;
  _rectangle.height = height;
  _rectangle.posX = posX;
  _rectangle.posY = posY;
  _rectangle.color;
  
  return _rectangle;
}

var leftScore = 0;
var rightScore = 0;

const action = {
  up : "up",
  down : "down"
}

var keysToPlayerActions = {
  "ArrowUp" :
  {
    activated : false,
    action : "RightUp"
  },
  "ArrowDown" :
  {
    activated : false,
    action : "RightDown"
  },
  "w" :
  { 
    activated : false,
    action : "LeftUp"
  },
  "s" :
  { 
    activated : false,
    action : "LeftDown"
  }
};

onkeydown = function(e) {
  var key = e.key;
  if (key in keysToPlayerActions) {
    keysToPlayerActions[key].activated = true;
  }
}

onkeyup = function(e) {
  var key = e.key;
  if (key in keysToPlayerActions) {
    keysToPlayerActions[key].activated = false;
  }
}

// document.addEventListener('keydown', event => {
//   var key = event.key;
//   //console.log(event);
  
//   if ((key in keysToPlayerActions) && !keysToPlayerActions[key].hit) {
//     keysToPlayerActions[key].hit = true;
//   }
//   // switch (key) {
//   //   case keysToPlayerActions.:
//   //     keysPressedStack.push(key);
//   //     //removeRectangle(paddleRight);
//   //     //movePaddle(paddleRight, action.up);
//   //     break;
//   //   case arrowDown:
//   //     //removeRectangle(paddleRight);
//   //     //movePaddle(paddleRight, action.down);
//   //     break;
//   //   case wKey:
//   //     //removeRectangle(paddleLeft);
//   //     //movePaddle(paddleLeft, action.up);
//   //     break;
//   //   case sKey:
//   //     //removeRectangle(paddleLeft);
//   //     //movePaddle(paddleLeft, action.down);
//   //     break;
//   // }
//   //drawRectangle(paddleLeft);
//   //drawRectangle(paddleRight);
// } );

// document.addEventListener('keypress', event => { 
//   console.log("keypress");
// });

// document.addEventListener('keyup', event => {
//   console.log(keysPressedStack);
//   keysPressedStack = [];
// });

// TODO: Wrap this in an object.
var leftWall = 0;
var upperWall = 0;
var lowerWall = 400;
var rightWall = 600;

const paddleHeight = 150;
const paddleWidth = 10;

var ball = createRectangle(10, 10);
var paddleLeft = createRectangle(paddleWidth, paddleHeight);
paddleLeft.color = 'white';
var paddleRight = createRectangle(paddleWidth, paddleHeight);
paddleRight.color = 'white';

function moveBall(ball, dt) {
  ball.posX += ball.velocityX * dt;
  ball.posY += ball.velocityY * dt;
}

function gameInit() {

  // Initialize the ball with a random direction but a fixed speed.
  var ballSpeed = 3;
  var randomAngle = Math.random() * 2 * Math.PI; // random number in [0, 2*pi)
  ball.velocityX = Math.round(Math.cos(randomAngle) * ballSpeed);
  ball.velocityY = Math.round(Math.sin(randomAngle) * ballSpeed);

  ball.color = 'white';
  ball.posX = rightWall / 2 - ball.width / 2;
  ball.posY = lowerWall / 2 - ball.height / 2;

  // Setup paddles.
  var paddleWallOffset = 20;
  paddleLeft.posX = paddleWallOffset
  paddleRight.posX = rightWall - (paddleRight.width + paddleWallOffset);
  paddleLeft.posY = lowerWall / 2 - paddleLeft.height / 2;
  paddleRight.posY = paddleLeft.posY;
}

function playSound() {
  var audio = document.createElement('audio');
  audio.src = 'sounds/pingpong.wav';
  audio.style.display = "none"; //added to fix ios issue
  audio.autoplay = false; //avoid the user has not interacted with your page issue
  audio.onended = function(){
    audio.remove(); //remove after playing to clean the Dom
  };
  document.body.appendChild(audio);
  audio.play();
}

// A time step in the game.
function gameStep(dt) {

  // Handle player inputs.
  Object.keys(keysToPlayerActions).forEach(function(key, i) {
    if (keysToPlayerActions[key].activated) {
      switch (keysToPlayerActions[key].action) {
        case "RightUp":
          movePaddle(paddleRight, action.up);
          break;
        case "RightDown":
          movePaddle(paddleRight, action.down);
          break;
        case "LeftUp":
          movePaddle(paddleLeft, action.up);
          break;
        case "LeftDown":
          movePaddle(paddleLeft, action.down);
          break;
      }
    }
  });

  moveBall(ball, dt);

  if (leftPaddleCollision(paddleLeft, ball) || 
      rightPaddleCollision(paddleRight, ball)) {
    ball.velocityX *= (-1);
    playSound();
    return false;
  }
  
  // Bounce on the upper and lower wall.
  if (ball.posY <= upperWall || ball.posY + ball.height >= lowerWall) {
    ball.velocityY *= (-1);
  }

  if (inRightGoal(ball, rightWall)) {
    rightScore++;
    //var rightScoreElement = document.getElementById("right-score");
    //rightScoreElement.innerText = rightScore;
    return true;
  }

  if (inLeftGoal(ball, leftWall)) {
    leftScore++;
    //var leftScoreElement = document.getElementById("left-score");
    //leftScoreElement.innerText = leftScore
    return true;
  }
}

function gameLoop() {
  isDone = false;
  gameInit();

  // The smallest possible time step in the game.
  var dt = 0.2;
  var step = setInterval(function() {
    isDone = gameStep(dt);
  }, dt);

  var fps = 120;
  var renderingLoop = setInterval(function () {
    clearScene();
    drawScene();
  }, 1000 / fps)

  if (isDone) {
    clearInterval(renderingLoop);
    clearInterval(gameStep);
  }
}

function movePaddle(paddle, inputAction) {
  // The number of pixels the paddle moves in the vertical direction.
  const paddleDeltaY = 5;

  switch (inputAction) {
    case action.up:
      paddle.posY -= paddleDeltaY;
      break;
    case action.down:
      paddle.posY += paddleDeltaY
      break;
  }
}

function verticalCollisionCondition(paddle, ball) {
  return paddle.posY <= ball.posY + ball.height &&
         ball.posY <= paddle.posY + paddle.height;
}

function leftPaddleCollision(paddle, ball) {
  var verticalCollision = verticalCollisionCondition(paddle, ball);
  var horizontalCollision = ball.posX <= paddle.posX + paddle.width &&
                            ball.posX + ball.width >= paddle.posX;

  return verticalCollision && horizontalCollision;
}

function rightPaddleCollision(paddle, ball) {
  var verticalCollision = verticalCollisionCondition(paddle, ball);
  var horizontalCollision = ball.posX + ball.width >= paddle.posX &&
                            ball.posX <= paddle.posX + paddle.width;
  
  return verticalCollision && horizontalCollision;
}

function inRightGoal(ball, rightWall) {
  return rightWall <= ball.posX + ball.width;
}

function inLeftGoal(ball, leftWall) {
  return ball.posX <= leftWall;
}

//////////  Visualization //////////
function clearScene() {
  var canvas = document.getElementById("mainCanvas");
  var ctx = canvas.getContext('2d');
  ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function drawScene() {
  drawBackground();
  drawRectangle(ball);
  drawRectangle(paddleLeft);
  drawRectangle(paddleRight);
}

function drawBackground() {
  var canvas = document.getElementById("mainCanvas");
  canvas.height = lowerWall;
  canvas.width = rightWall;

  var ctx = canvas.getContext("2d");
  ctx.fillStyle = 'black';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function drawRectangle(rect) {
  var canvas = document.getElementById("mainCanvas");
  
  var ctx = canvas.getContext("2d");
  ctx.fillStyle = rect.color;
  ctx.fillRect(rect.posX, rect.posY, rect.width, rect.height);
}

// function removeRectangle(rect) {
//   var canvas = document.getElementById("mainCanvas");
//   var ctx = canvas.getContext("2d");
//   ctx.fillStyle = 'black';
//   ctx.fillRect(rect.posX, rect.posY, rect.width, rect.height);
// }

drawBackground();
gameLoop();